function filterBy(data, type) {
  let arr = [];
  data.forEach(function (e) {
    if (typeof e !== type) {
      arr.push(e);
    }
  });
  return arr;
}
let all = ["hello", "world", 23, "23", null];
alert(filterBy(all, "string"));

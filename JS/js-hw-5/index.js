//Экранирование символов — замена в тексте управляющих символов на соответствующие текстовые подстановки

let name = prompt("Введите имя", "");
let lastName = prompt("Введите фамилию", "");
let birthday = prompt("Введите дату рождения", "dd.mm.yyyy");

function createNewUser(name, lastName, birthday) {
  return {
    name: name,
    lastName: lastName,
    getLogin: function () {
      return name.charAt(0).toLowerCase() + lastName.toLowerCase();
    },
    getAge: function () {
      return new Date() - birthday;
    },
    getPassword: function () {
      return (
        name.charAt(0).toUpperCase() +
        lastName.toLowerCase() +
        birthday.slice(-4)
      );
    },
  };
}

let newUser = createNewUser(name, lastName, birthday);
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());

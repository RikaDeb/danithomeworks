let images = ["1.jpg", "2.jpg", "3.jpg", "4.jpg"];

let slider = document.querySelector("#slider");
let img = slider.querySelector("img");

let i = 1;
img.src = "img/" + images[0];
let timerId = window.setInterval(function () {
  img.src = "img/" + images[i];
  i++;
  if (i == images.length) {
    i = 0;
  }
}, 3000);

document.querySelector(".button-stop").addEventListener("click", function () {
  clearInterval(timerId);
});

document.querySelector(".button-start").addEventListener("click", function () {
  timerId = window.setInterval(function () {
    img.src = "img/" + images[i];
    i++;
    if (i == images.length) {
      i = 0;
    }
  }, 3000);
});

let tab = function () {
  let tabNav = document.querySelectorAll(".tabs-nav_item"),
    tabContent = document.querySelectorAll(".tab"),
    tabName;

  tabNav.forEach((item) => {
    item.addEventListener("click", selectTabNav);
  });

  function selectTabNav() {
    tabNav.forEach((item) => {
      item.classList.remove("is-active");
    });
    this.classList.add("is-active");
    tabName = this.getAttribute("data-tab-name");
    selectTabContent(tabName);
  }

  function selectTabContent(tabName) {
    tabContent.forEach((item) => {
      item.classList.contains(tabName)
        ? item.classList.add("is-active")
        : item.classList.remove("is-active");
    });
  }
};

tab();

// let secTab = function () {
//   let secTabNav = document.querySelectorAll(".sec-tabs-nav_item"),
//     secTabContent = document.querySelectorAll(".sec-tab"),
//     secTabName;

//   secTabNav.forEach((secItem) => {
//     secItem.addEventListener("click", secSelectTabNav);
//   });

//   function secSelectTabNav() {
//     secTabNav.forEach((secItem) => {
//       secItem.classList.remove("sec-is-active");
//     });
//     this.classList.add("sec-is-active");
//     secTabName = this.getAttribute("data-tab-name");
//     secSelectTabContent(secTabName);
//   }

//   function secSelectTabContent(secTabName) {
//     secTabContent.forEach((secItem) => {
//       secItem.classList.contains(secTabName)
//         ? secItem.classList.add("sec-is-active")
//         : secItem.classList.remove("sec-is-active");
//     });
//   }
// };

// secTab();

// $(document).ready(function () {
//   $(".slider").slick({
//     arrows: true,
//     dots: false,
//     adaptiveHeight: true,
//     slidesToShow: 4,
//     slidesToScroll: 1,
//     infinite: true,
//     draggable: false,
//     centerMode: true,
//     asNavFor: ".sliderbig",
//   });
//   $(".sliderbig").slick({
//     arrows: false,
//     asNavFor: ".slider",
//   });
// });

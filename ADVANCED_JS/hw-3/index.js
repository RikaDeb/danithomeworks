class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  get employeeName() {
    return this.name;
  }
  get employeeAge() {
    return this.age;
  }
  get employeeSalary() {
    return this.salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary, lang);
    this.lang = lang;
  }
  get programmerLang() {
    return this.lang;
  }
  get salaryInfo() {
    return this.salary * 3;
  }
}

let programmer = new Programmer("Jonh", 25, 5000, "English, Russian");

console.log(programmer.salaryInfo);
console.log(programmer.programmerLang);
console.log(programmer);

const clients1 = [
  "Гилберт",
  "Сальваторе",
  "Пирс",
  "Соммерс",
  "Форбс",
  "Донован",
  "Беннет",
];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

let result = function (surname) {
  let tmp = {};
  return surname.filter(function (a) {
    return a in tmp ? 0 : (tmp[a] = 1);
  });
};

const surname = [...clients1, ...clients2];

console.log(result(surname));

const books = [
  {
    name: "Harry Potter",
    author: "J.K. Rowling",
  },
  {
    name: "Lord of the rings",
    author: "J.R.R. Tolkien",
  },
  {
    name: "The witcher",
    author: "Andrzej Sapkowski",
  },
];

const bookToAdd = {
  name: "Ink heart",
  author: "Cornelia Funke",
};

const bookToAdd = {
  name: "Ink heart",
  author: "Cornelia Funke",
};

const allbooks = [{ ...books, ...bookToAdd }];

console.log(allbooks);

const user1 = {
  name: "John",
  years: 30,
};

const { name: Name, years: age, isAdmin = false } = user1;

alert(Name);
alert(age);
alert(isAdmin);
